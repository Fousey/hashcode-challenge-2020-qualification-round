# HashCode challenge 2020 - Qualification round
![](data/hashcode.jpeg)

## Online Qualification Round
The problem statement (PDF) : Hashcode_2020_online_qualification_round.pdf

## Project structure
### data 
all inputs files are listed in this folder
### solution
this folder contains the output files : the solution submitted to Google - Result : 22,713,788
### src
Problem algorithm.

