import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Library {

    private int id;
    private int numBooks;
    final int signupTime;
    private int booksPerDay;
    private Set<Book> books = new HashSet<>();
    List<Book> booksOrder = new ArrayList<>();

    public Library(int id, int numBooks, int signupTime, int booksPerDay) {
        this.id = id;
        this.numBooks = numBooks;
        this.signupTime = signupTime;
        this.booksPerDay = booksPerDay;
    }
    public Library cloneSolution() {
        Library library = new Library(id, numBooks, signupTime, booksPerDay);
        library.booksOrder.addAll(booksOrder);
        return library;
    }

    public int canStillScan(int day, int numDays) {
        long booksToScan = (numDays - day) * (long) booksPerDay;
        long val = Math.min(booksOrder.size(), booksToScan);
        return (int) val;
    }
    
// -------------- GETTERS AND SETTERS ----------------
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumBooks() {
        return numBooks;
    }

    public void setNumBooks(int numBooks) {
        this.numBooks = numBooks;
    }

    public int getBooksPerDay() {
        return booksPerDay;
    }

    public void setBooksPerDay(int booksPerDay) {
        this.booksPerDay = booksPerDay;
    }

    public List<Book> getBooksOrder() {
        return booksOrder;
    }

    public void setBooksOrder(List<Book> booksOrder) {
        this.booksOrder = booksOrder;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }
}
