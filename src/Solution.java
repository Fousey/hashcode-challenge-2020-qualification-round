import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution {

    public static final String JUMP = "\n";
    public static final String SPACE = " ";

    List<Library> libraries = new ArrayList<>();

    private final Problem problem;

    public void sortLibrariesOnTime() {
        libraries.sort(Comparator.comparingInt(o -> o.signupTime));
    }

    public void sortLibrariesOnScoreLeft() {
        addAllBooks();
        Set<Library> libsLeft = new HashSet<>(problem.getLibraries());
        Set<Book> justAdded = new HashSet<>();
        int day = 0;
        // sorting books by value
        libsLeft.forEach(l -> l.booksOrder.sort(Comparator.comparingInt(o -> -o.getValue())));

        while (!libsLeft.isEmpty() && day < problem.getNumDays()) {
            Library bestLibrary = null;
            int bestScore = -1;

            for (Library library : libsLeft) {
                library.booksOrder.removeAll(justAdded);
                int score = 0;
                Set<Book> booksAdd = new HashSet<>();
                for (int i = 0; i < library.canStillScan(day, problem.getNumDays()); i++) {
                    score += library.booksOrder.get(i).getValue();
                    booksAdd.add(library.booksOrder.get(i));
                }
                if (score > bestScore) {
                    bestLibrary = library;
                    bestScore = score;
                    justAdded = booksAdd;
                }
            }

            libsLeft.remove(bestLibrary);
            libraries.add(bestLibrary);
            day += bestLibrary.signupTime;
        }
    }
    public void sortOnBooksLeft() {
        Set<Book> booksScanned = new HashSet<>();
        int day = 0;
        for (Library library : libraries) {
            day += library.signupTime;

            if (day >= problem.getNumDays()) {
                break;
            }

            library.booksOrder.sort((o1, o2) -> {
                if (booksScanned.contains(o1) == booksScanned.contains(o2)) {
                    return o2.getValue() - o1.getValue();
                } else {
                    if (booksScanned.contains(o1)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });

            for (int i = 0; i < library.canStillScan(day, problem.getNumDays()); i++) {
                booksScanned.add(library.booksOrder.get(i));
            }
        }
    }

    public void addAllLibraries() {
        libraries = new ArrayList<>(problem.getLibraries());
    }

    public void addAllBooks() {
        for (Library library : libraries) {
            library.booksOrder = new ArrayList<>(library.getBooks());
        }
    }

    public Solution(Problem problem) {
        this.problem = problem;
    }
    
    private long calcScore() {
        int day = 0;
        Set<Book> booksScanned = new HashSet<>();
        Set<Library> librariesSeen = new HashSet<>();
        for (Library library : libraries) {
            if (librariesSeen.contains(library)) {
                throw new IllegalStateException("Library exists multiple times in solution.");
            }
            librariesSeen.add(library);

            day += library.signupTime;
            if (day >= problem.getNumDays()) {
                break;
            }
            for (int i = 0; i < library.canStillScan(day, problem.getNumDays()); i++) {
                booksScanned.add(library.booksOrder.get(i));
            }
        }
        long score = 0;
        for (Book book : booksScanned) {
            score += book.getValue();
        }
        return score;
    }

    
    /** 
     * @return long
     */
    public long getScore() {
        return calcScore();
    }

    /**
     * print result on file
     */
    public void save() {
        try {

            BufferedWriter writer = new BufferedWriter(new FileWriter("hashcode-challenge-2020-qualification-round/solution/" + problem.getName()));
            writer.write(libraries.size() + JUMP);
            for (Library library : libraries) {
                writer.write(library.getId() + SPACE + library.booksOrder.size() + JUMP);
                for (Book book : library.booksOrder) {
                    writer.write(book.getId() + SPACE);
                }
                writer.write(JUMP);
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
