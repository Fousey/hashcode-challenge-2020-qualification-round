import java.util.HashSet;
import java.util.Set;

public class Book {

    private int id;
    private int value;
    private Set<Library> libraries = new HashSet<>();

    public Book(int id, int value) {
        this.id = id;
        this.value = value;
    }

    // -------------- GETTERS AND SETTERS ----------------
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Set<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(Set<Library> libraries) {
        this.libraries = libraries;
    }
}
