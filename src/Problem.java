import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem {
    
    private String name;
    private int numBooks;
    private int numLibs;
    private int numDays;
    private ArrayList<Book> books;
    private ArrayList<Library> libraries;
    
    public Problem(String name) {
        this.name = name;
        Scanner sc = null;
        try {
            sc = new Scanner(new File("hashcode-challenge-2020-qualification-round/data/" + name));
            this.numBooks = sc.nextInt();
            this.numLibs = sc.nextInt();
            this.numDays = sc.nextInt();
            
            books = new ArrayList<>();
            for (int i = 0; i < numBooks; i ++) {
                books.add(new Book(i, sc.nextInt()));
            }
            
            libraries = new ArrayList<>();
            for (int i = 0; i < numLibs; i ++) {
                Library library = new Library(i, sc.nextInt(), sc.nextInt(), sc.nextInt());
                for (int j = 0; j < library.getNumBooks(); j ++) {
                    Book book = books.get(sc.nextInt());
                    library.getBooks().add(book);
                    book.getLibraries().add(library);
                }
                libraries.add(library);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        finally{
            sc.close();
        }
        
    }
    
    // -------------- GETTERS AND SETTERS ----------------
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getNumBooks() {
        return numBooks;
    }
    
    public void setNumBooks(int numBooks) {
        this.numBooks = numBooks;
    }
    
    public int getNumLibs() {
        return numLibs;
    }
    
    public void setNumLibs(int numLibs) {
        this.numLibs = numLibs;
    }
    
    public int getNumDays() {
        return numDays;
    }
    
    public void setNumDays(int numDays) {
        this.numDays = numDays;
    }
    
    public ArrayList<Library> getLibraries() {
        return libraries;
    }
    
    public void setLibraries(ArrayList<Library> libraries) {
        this.libraries = libraries;
    }
    
    public ArrayList<Book> getBooks() {
        return books;
    }
    
    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }
    
}
