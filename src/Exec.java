
/**                                                                                         
     __    __       ___           _______. __    __    ______   ______    _______   _______   
    |  |  |  |     /   \         /       ||  |  |  |  /      | /  __  \  |       \ |   ____|  
    |  |__|  |    /  ^  \       |   (----`|  |__|  | |  ,----'|  |  |  | |  .--.  ||  |__     
    |   __   |   /  /_\  \       \   \    |   __   | |  |     |  |  |  | |  |  |  ||   __|    
    |  |  |  |  /  _____  \  .----)   |   |  |  |  | |  `----.|  `--'  | |  '--'  ||  |____   
    |________|____/ __  \__\ ________/ __ |__|  ___|  \__________.____/__.___________________ 
     /      ||  |  |  |     /   \     |  |     |  |     |   ____||  \ |  |  /  _____||   ____|
    |  ,----'|  |__|  |    /  ^  \    |  |     |  |     |  |__   |   \|  | |  |  __  |  |__   
    |  |     |   __   |   /  /_\  \   |  |     |  |     |   __|  |  . `  | |  | |_ | |   __|  
    |  `----.|  |  |  |  /  _____  \  |  `----.|  `----.|  |____ |  |\   | |  |__| | |  |____ 
     \______||__|  |__| /__/    ____\ |_______|_________________||__| \__|  \______| |_______|
                               |__ \   / _ \  |__ \   / _ \                                   
                                  ) | | | | |    ) | | | | |                                  
                                 / /  | | | |   / /  | | | |                                  
                                / /_  | |_| |  / /_  | |_| |                                  
                               |____|  \___/  |____|  \___/                                   
 */
public class Exec {
 
    Problem pbA, pbB, pbC, pbD, pbE, pbF;   
    boolean processA, processB, processC, processD, processE, processF;

    public static void main(String[] args) {
        Exec exec = new Exec();
        exec.init();
        exec.process();
    }

    public void init() {
        processA = true;
        processB = true;
        processC = true;
        processD = true;
        processE = true;
        processF = true;
    }

    public Solution solve(Problem problem) {
        Solution solution;
        solution = sortLibsOnStartTime(problem);
        return solution;
    }

    public Solution sortLibsOnStartTime(Problem problem) {
        Solution solution = new Solution(problem);

        solution.addAllLibraries();
        solution.addAllBooks();

        solution.sortLibrariesOnTime();
        solution.sortOnBooksLeft();
        return solution;
    }

    public void process() {
        if (processA) {
            pbA = new Problem("a_example.txt");
            Solution a = solve(pbA);
            System.out.println("A: " + a.getScore());
            a.save();
        }
        if (processB) {
            pbB = new Problem("b_read_on.txt");
            Solution b = solve(pbB);
            System.out.println("B: " + b.getScore());
            b.save();
        }
        if (processC) {
            pbC = new Problem("c_incunabula.txt");
            Solution c = solve(pbC);
            System.out.println("C: " + c.getScore());
            c.save();
        }
        if (processD) {
            pbD = new Problem("d_tough_choices.txt");
            Solution d = solve(pbD);
            System.out.println("D: " + d.getScore());
            d.save();
        }
        if (processE) {
            pbE = new Problem("e_so_many_books.txt");
            Solution e = solve(pbE);
            System.out.println("E: " + e.getScore());
            e.save();
        }
        if (processF) {
            pbF = new Problem("f_libraries_of_the_world.txt");
            Solution f = solve(pbF);
            System.out.println("F: " + f.getScore());
            f.save();
        }
    }
}
